def buildJar() {
    echo "building the java maven application"
    sh "mvn package"
}

def buildDockerImage() {
    echo "fabricating docker image..."
    sh "docker build -t gagupta92/demo_repo:jma-${params.app_version} ."
}

def pushDockerImage() {
    echo "pushing docker image to dockerhub"
    // withCredentials([usernamePassword(credentialsId: 'gg1992_docker', usernameVariable: 'DOCKER_USER', passwordVariable: 'DOCKER_PASSWD')]) {
    // sh('echo $DOCKER_PASSWD | docker login -u $DOCKER_USER --password-stdin') // https://www.jenkins.io/doc/book/pipeline/jenkinsfile/#string-interpolation
    sh('docker login -u $GIT_CREDS_USR -p $GIT_CREDS_PSW')
    sh "docker push gagupta92/demo_repo:jma-${params.app_version}"
    // }
}

return this
